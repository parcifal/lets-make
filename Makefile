# LETS-MAKE 0.0.1
# 2024 (c) M.P. van de Weerd
#
# https://www.van-de-weerd.net/~michael/
# https://gitlab.com/parcifal/lets-make/

# This Makefile provide a convenient interface for certbot, allowing the 
# generation of Let's Encrypt SSL certificates (certs). The point of this
# Makefile is to allow easy and safe handling of large amounts of domains.
# To do so, all domains are specified in a separate file. Wildcard domains
# are allowed but discouraged.
#
# An INDIVIDUAL CERT is generated for EACH DOMAIN. Having alt domains exposes
# information of associated domains to the public. This is not a problem in
# most cases, but it can be one in some, which is easily prevented by this
# measure. HTTP-servers such as Nginx support such a setup by exposing the
# server name in the configuration. For example, all domains can be configured
# by default in Nginx like this:
#
#  > ssl_certificate     ./ssl/$ssl_server_name/fullchain.pem;
#  > ssl_certificate_key ./ssl/$ssl_server_name/privkey.pem;
#
# This does however not work for wildcard certs, hence they need to be
# configured on a case-by-case basis.

# A global configuration file, containing all options passed to Certbot. See
# the following link for its formal specification:
# https://eff-certbot.readthedocs.io/en/stable/using.html#configuration-file
CERTBOT_CONFIG = certbot.conf

# The directory in which the key and chain files will be stored. For each
# domain, a subdirectory is created containing the key and chain files.
CERT_DIR = ssl

# A temporary directory in which the Certbot output will be stored.
TEMP_DIR = .certbot

# The file in which the domains are specified for which a key and cert must
# be generated. Each domain name is separated by some form of whitespace.
# Wildcard subdomains can be specified with an asterisk (*.example.com).
DOMAINS_FILE = domains.txt

# The domains for which a key and chain must be generated.
DOMAINS = $(shell cat $(DOMAINS_FILE))

# !!! DO NOT CHANGE !!!
# The names of the certificates. Each certificate is named identical its
# associated domain. Wildcard domains (*.example.com) are stored with an
# underscore instead of an asterisk (_.example.com), due to filename
# limitations.
CERTS = $(subst *, _, $(DOMAINS))

# !!! DO NOT CHANGE !!!
# The directory in which Certbot stores the key and chain files.
LIVE_DIR = $(TEMP_DIR)/config/live

# !!! DO NOT CHANGE !!!
# The name of the key file.
KEY_FILE = privkey.pem

# !!! DO NOT CHANGE !!!
# The name of the chain file.
FULLCHAIN_FILE = fullchain.pem

.PHONY: all renew clean
.PRECIOUS: $(LIVE_DIR)/%

# Generate a cert for each specified domain.
all: $(addprefix $(CERT_DIR)/, $(CERTS))
	chown -R $$USER:$$USER $(CERT_DIR)

# Revoke all certificates and remove all associated files.
clean:
	for CERT in $(CERTS); do \
	    certbot revoke \
	        --config $(CERTBOT_CONFIG) \
		--cert-name $$CERT \
	        --delete-after-revoke; \
	done
	rm -rf $(TEMP_DIR)
	rm -rf $(CERT_DIR)

# Renew all certificates that require renewal.
renew:
	certbot renew \
	    --config $(CERTBOT_CONFIG) \
	    --force-renewal
	cp -rfL $(LIVE_DIR)/* $(CERT_DIR)/
	chmod -R 664 $(CERT_DIR)/*/*

# Create a cert directory.
$(CERT_DIR)/%: $(LIVE_DIR)/%
	mkdir -pm 755 $@
	cp -fL $</* $@
	chmod 744 $@/*

# Generate a cert.
$(LIVE_DIR)/%:
	certbot certonly \
	    --config $(CERTBOT_CONFIG) \
	    --cert-name $* \
	    --domains "$(subst _, *, $*)"

