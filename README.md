# Let's Make

This Makefile provides a convenient interface for certbot, allowing the 
generation of Let's Encrypt SSL certificates (certs). The point of this
Makefile is to allow easy and safe handling of large amounts of domains.
To do so, all domains are specified in a separate file. Wildcard domains
are allowed but discouraged.

An **individual cert** is generated for **each domain**. Having alt domains
exposes information of associated domains to the public. This is not a problem
in most cases, but it can be one in some, which is easily prevented by this
measure. HTTP-servers such as Nginx support such a setup by exposing the
server name in the configuration. For example, all domains can be configured
by default in Nginx like this:

```nginx
ssl_certificate     ./ssl/$ssl_server_name/fullchain.pem;
ssl_certificate_key ./ssl/$ssl_server_name/privkey.pem;
```

This does however not work for wildcard certs, hence they need to be
configured on a case-by-case basis.

## Usage

Before running the `make` commands, the domains for which you want to generate 
the certs need to be specified. By default, this is done in a `domains.txt`
file. Domains are separated by any type of whitespace (e.g. spaces, tabs and
newlines). Wildcard subdomains are specified with an asterisk.

The following is an example of a specification in a `domains.txt`:

```
example.com     www.example.com
                app.example.com

                *.staging.example.com
```

In order to generate a cert for each of the domains that you have specified,
simply run `make`. To renew all certs that are due for renewal, run
`make renew`. To revoke all certs and remove all associated files and 
directories, run `make clean`.

Taking the earlier example, the following certs are generated in the
cert directory (which is `ssl` by default):

```
./ssl/example.com/
./ssl/www.example.com/
./ssl/app.example.com/
./ssl/_.staging.example.com/
```

 > **Note that wildcard certs are stored with an *underscore* instead of an
 > asterisk!**

## Requirements

In order to use this Makefile, `make` must be installed. `make` is available
on most package managers, e.g. on Debian:

```shell
sudo apt install make
```

Additionally, Certbot must be installed. Use the instruction provided 
[here][certbot] in order to install it properly on your system.

[certbot]: https://certbot.eff.org/instructions

